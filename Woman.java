package homework6;

import java.util.Arrays;

final class Woman extends Human {


    public Woman(){}
    public Woman(String name,String surname,int year){
        super(name,surname,year);
    }
    public Woman(String name, String surname, int year, int iq, Pet pet, String [][] schedule){
       super(name,surname,year,iq,pet,schedule);
    }
    public void makeup(){System.out.println("подкраситься");}

    @Override
    public  String toString (){
        String message = "Woman{name = " + getName() + " surname = " + getSurname() +" year = "+ getYear() + " iq = " + getIq() + " schedule = " + Arrays.deepToString(getSchedule()) +
                "}" ;
        System.out.println(message);
        return message;
    }
}
