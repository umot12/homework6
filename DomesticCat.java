package homework6;

import java.util.Arrays;

public class DomesticCat extends Pet{
    private final Species species = Species.DomesticCat;

    public DomesticCat(String nickname){
        super(nickname);
    }
    public DomesticCat(String nickname,int age,int trickLevel,String [] habits){
        super(nickname,age,trickLevel,habits);
    }
    public DomesticCat(String nickname,int age,int trickLevel){
        super(nickname,age,trickLevel);
    }
    public DomesticCat(){}

    @Override
    public void respond() {
        System.out.println("хояин я " + getNickname() + ". Я соскучился");
    }

    @Override
    public   String  toString(){

        String message =  species + "{nickname=" + getNickname() +", age=" + getAge() +", trickLevel=" + getTrickLevel() +" , habits=" + Arrays.toString(getHabits()) + "}" ;
        System.out.println(message);
        return message;
    }
    }



