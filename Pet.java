package homework6;


import java.util.Arrays;

public abstract class Pet {
    private Species species = Species.UNKNOWN;
    private String nickname;
    private int age;
    private int trickLevel;
    private String[] habits;

    enum Species {
        DOG,CAT,Fish,DomesticCat,RoboCat,UNKNOWN;
    }
    public Pet(String nickname){

        this.nickname = nickname;
    }
    public Pet(String nickname,int age,int trickLevel,String [] habits){
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }
    public Pet(String nickname,int age,int trickLevel){
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;

    }
    public Pet(){}
    public int getAge() {return age;}

    public String[] getHabits() {
        return habits;
    }

    public void setHabits(String[] habits) {
        this.habits = habits;
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Species getSpecies() {
        return species;
    }

    public void setSpecies(Species species) {
        this.species = species;
    }

    public void eat() {
        System.out.println("я кушаю!");
    }

    public abstract void respond(); {

    }
//    @Override
//    public void foul() {
//        System.out.println("Нужно хорошо замести следы...");
//    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }
    @Override
    protected void finalize(){
        System.out.println("Object Pet deleted");
    }

    @Override
    public   String  toString(){

        String message =  species + "{nickname=" + nickname +", age=" + age +", trickLevel=" + trickLevel +" , habits=" + Arrays.toString(habits) + "}" ;
        System.out.println(message);
        return message;
    }

    public static void main  (String [] Args)  {


        String[] hobbyHach= {"not run", "only sleep"};
        Dog d = new Dog("dd",2,32,hobbyHach);
        d.toString();
        d.eat();
        System.out.println();
        String[] hobbyHach1= {"not run", "only sleep"};
        RoboCat rc = new RoboCat("dd",2,32,hobbyHach1);
        rc.toString();
        rc.foul();
        rc.eat();
    }

}
interface FFoul{
  void foul();
}