package homework6;

import org.junit.jupiter.params.shadow.com.univocity.parsers.csv.CsvParser;

import java.util.Arrays;

public class RoboCat extends Pet implements FFoul{
   private final Species species = Species.RoboCat;

    public RoboCat(String nickname){
        super(nickname);
    }
    public RoboCat(String nickname,int age,int trickLevel,String [] habits){
        super(nickname,age,trickLevel,habits);
    }
    public RoboCat(String nickname,int age,int trickLevel){
        super(nickname,age,trickLevel);
    }
    public RoboCat(){}

    @Override
    public void foul() {
        System.out.println("Нужно хорошо замести следы...");
    }
    @Override
    public void respond() {
        System.out.println("хояин я " + getNickname() + ". Я соскучился");
    }

    @Override
    public   String  toString(){

        String message =  species + "{nickname=" + getNickname() +", age=" + getAge() +", trickLevel=" + getTrickLevel() +" , habits=" + Arrays.toString(getHabits()) + "}" ;
        System.out.println(message);
        return message;
    }
}

