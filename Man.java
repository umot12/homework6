package homework6;

import java.util.Arrays;

public class Man extends Human{

    public Man(){}
    public Man(String name,String surname,int year){
        super(name,surname,year);
    }
    public Man(String name, String surname, int year, int iq, Pet pet, String [][] schedule){
        super(name,surname,year,iq,pet,schedule);
    }
    public void repairCar(){System.out.println(getName()+" иди чинить авто");}
    @Override
    public  String toString (){
        String message = "Man{name = " + getName() + " surname = " + getSurname() +" year = "+ getYear() + " iq = " + getIq() + " schedule = " + Arrays.deepToString(getSchedule()) +
                "}" ;
        System.out.println(message);
        return message;
    }
}
