package homework6;

import java.util.Arrays;

public class Dog extends Pet implements FFoul{
    private final Species species = Species.DOG;

    public Dog(String nickname){
        super(nickname);
    }
    public Dog(String nickname,int age,int trickLevel,String [] habits){
        super(nickname,age,trickLevel,habits);
    }
    public Dog(String nickname,int age,int trickLevel){
        super(nickname,age,trickLevel);
    }
    public Dog(){}

    @Override
    public void respond() {
        System.out.println("хояин я " + getNickname() + ". Я соскучился");
    }
    @Override
    public   String  toString(){

        String message = species + "{nickname=" + getNickname() +", age=" + getAge() +", trickLevel=" + getTrickLevel() +" , habits=" + Arrays.toString(getHabits()) + "}" ;
        System.out.println(message);
        return message;
    }

    @Override
    public void foul() {
        System.out.println("Нужно хорошо замести следы...");
    }
}
class s{
    public static void main(String[] args) {
        String[] hobbyHach= {"not run", "only sleep"};
        Dog d = new Dog("dd",2,32,hobbyHach);
        d.toString();
    }}